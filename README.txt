Downloads a YouTube video given the URL and converts the MP4 to MP3, 
great for audiobooks, songs, and anything else you want from YouTube.