﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using YoutubeExtractor;

namespace YouTube_Grabber
{
    class Program
    {
        private static string Resolution = "1080";

        private static void ConvertToMP3(IEnumerable<VideoInfo> videoInfos)
        {
            VideoInfo video;
            try { video = videoInfos.First(info => info.VideoType == VideoType.Mp4 && info.Resolution == 360); }
            catch { video = null; }

            if (video == null)
            {
                Console.WriteLine("Video Info extraction failed, will not continue.");
                return;
            }

            string videoInputPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                    RemoveIllegalPathCharacters(video.Title.Replace(" ", "")) + video.VideoExtension);

            string audioOutputPath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                    RemoveIllegalPathCharacters(video.Title.Replace(" ", "")) + ".mp3");

            Process process = new Process();
            try
            {
                string arguments = "-i \"" + videoInputPath + "\" -q:a 192 -map a? \"" + audioOutputPath + "\"";
                System.Security.SecureString password = VerifyPassword();

                Process.Start(@"ffmpeg\bin\ffmpeg.exe", arguments, Environment.UserName, password, Environment.UserDomainName);
            }
            catch (Exception ex)
            {
                Console.WriteLine("\nConversion to MP3 failed.");
                Console.WriteLine("Message: " + ex.Message);
            }
        }

        private static System.Security.SecureString VerifyPassword()
        {
            System.Security.SecureString password = new System.Security.SecureString();
          
            if (!File.Exists("password.pwd"))
            {
                Console.Write("\nPassword: ");
                string input = Console.ReadLine();
                foreach (var c in input)
                    password.AppendChar(c);

                File.WriteAllText("password.pwd", input);
            }
            else
            {
                string input = File.ReadAllText("password.pwd");
                foreach (var c in input)
                    password.AppendChar(c);
            }

            return password;
        }

        private static void DownloadAudio(IEnumerable<VideoInfo> videoInfos)
        {
            try
            {
                /*
                 * We want the first extractable video with the highest audio quality.
                 */

                //VideoInfo video = videoInfos.OrderByDescending(info => info.AudioBitrate).First();

                VideoInfo video = videoInfos
                    .Where(info => info.CanExtractAudio)
                    .OrderByDescending(info => info.AudioBitrate)
                    .First();

                /*
                 * If the video has a decrypted signature, decipher it
                 */
                if (video.RequiresDecryption)
                {
                    DownloadUrlResolver.DecryptDownloadUrl(video);
                }

                /*
                 * Create the audio downloader.
                 * The first argument is the video where the audio should be extracted from.
                 * The second argument is the path to save the audio file.
                 */

                var audioDownloader = new AudioDownloader(video,
                    Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                    RemoveIllegalPathCharacters(video.Title) + video.AudioExtension));

                // Register the progress events. We treat the download progress as 85% of the progress
                // and the extraction progress only as 15% of the progress, because the download will
                // take much longer than the audio extraction.
                audioDownloader.DownloadProgressChanged += (sender, args) => Console.Write("\rDownloading Audio... {0}%", Math.Round(args.ProgressPercentage * 0.85));
                audioDownloader.AudioExtractionProgressChanged += (sender, args) => Console.Write("\rDownloading Audio... {0}%", Math.Round(85 + args.ProgressPercentage * 0.15));

                /*
                 * Execute the audio downloader.
                 * For GUI applications note, that this method runs synchronously.
                 */
                audioDownloader.Execute();
            }
            catch (Exception ex)
            {
                Console.WriteLine("Audio extraction failed");
                Console.WriteLine("Message: " + ex.Message);
                return;
            }
        }

        private static void DownloadVideo(IEnumerable<VideoInfo> videoInfos)
        {
            VideoInfo video;
            /*
             * Select the first .mp4 video with 1080p, but default to 360p resolution
             */
            int result = 360;
            bool success = int.TryParse(Resolution, out result);
            int resolution = 360;

            if (success)
                resolution = result;
            else
                resolution = 360;

            //try
            //{
            //    video = videoInfos.First(info => info.VideoType == VideoType.Mp4 && info.Resolution == resolution && info.AudioBitrate > 0);
            //}
            //catch {
            video = videoInfos.First(info => info.VideoType == VideoType.Mp4 && info.AudioBitrate > 0 && info.Resolution == 360);
            //}

            /*
             * If the video has a decrypted signature, decipher it
             */
            if (video.RequiresDecryption)
            {
                DownloadUrlResolver.DecryptDownloadUrl(video);
            }

            /*
             * Create the video downloader.
             * The first argument is the video to download.
             * The second argument is the path to save the video file.
             */
            var videoDownloader = new VideoDownloader(video,
                Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.Desktop),
                RemoveIllegalPathCharacters(video.Title.Replace(" ", "")) + video.VideoExtension));

            // Register the ProgressChanged event and print the current progress
            videoDownloader.DownloadProgressChanged += (sender, args) => Console.Write("\rDownloading Video... {0}%", Math.Round(args.ProgressPercentage));

            /*
             * Execute the video downloader.
             * For GUI applications note, that this method runs synchronously.
             */
            videoDownloader.Execute();
        }

        private static void Main(string[] args)
        {
            string answer = "y";
            string link = string.Empty;

            while (answer.ToLower().Contains("y") || string.IsNullOrEmpty(answer))
            {
                Resolution = "1080";
                Console.Write("YouTube URL: ");
                if (args.Length > 0)
                {
                    Console.Write(args[0] + "\n");
                    link = args[0];
                }
                else
                    link = Console.ReadLine();

                IEnumerable<VideoInfo> videoInfos = DownloadUrlResolver.GetDownloadUrls(link, false);

                if (videoInfos == null) continue;

                //DownloadAudio(videoInfos);
                try { DownloadVideo(videoInfos); }
                catch
                {
                    try { DownloadVideo(videoInfos); }
                    catch
                    {
                        Console.WriteLine("\nDownload failed for unknown reason, please close the program and try again.");
                    }
                }

                if (args.Length < 2)
                    ConvertToMP3(videoInfos);

                answer = Continue(args);
            }
        }

        private static string Continue(string[] args)
        {
            if (args.Length > 0) return "n";

            Console.WriteLine("\nDownload another URL? [Y]");
            return Console.ReadLine().ToLower();
        }

        private static string RemoveIllegalPathCharacters(string path)
        {
            string regexSearch = new string(Path.GetInvalidFileNameChars()) + new string(Path.GetInvalidPathChars());
            var r = new Regex(string.Format("[{0}]", Regex.Escape(regexSearch)));
            return r.Replace(path, "");
        }
    }
}
